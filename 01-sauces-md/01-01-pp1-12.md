---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

# ソース {#sauces}

## フォン、その他のストック  {#les-fonds-de-cuisine}

\index{fonds@fonds}
\index{ふおん@フォン}

本書は実際に厨房で働く料理人を対象としたものだが、まず最初に料理のベー
スとして仕込んでストックしておくもの[^01]について少々述べておきた
い[^02]。我々料理人にとって重要なものだからだ。

[^01]: 原文 fonds （フォン）。本書での fonds の語は fond （基礎、
    土台）、fonds （資産、資本）、そして料理用語として一般に用いられて
    いるフォン、の 3 重の意味をかけている。つまり、液体（出汁）にとど
    まらず、固形、半固形のものも含まれる。そのまま「フォン」と訳したい
    ところだが、日本語の場合「出汁」の意味合いが強いため、分かりやすさ
    を重視してやや冗長に「料理のベースとして仕込んでストックしておくも
    の」と訳している。この概念そのものはエスコフィエ自身のアイデアとは
    いい難く、彼が目標としていたデュボワとベルナールの『キュイジーヌ・
    クラシック』第二版において明確な説明はないものの、内容を熟読すれば
    既にほぼ完成されていたことが分かる。また、この概説部分については第
    二版以降大幅に加筆修正がなされている。


[^02]: この部分は経営者に向けて書かれているようにも読めるが、エス
    コフィエの時代以降、料理人がオーナーシェフとして経営に携わるケース
    が激増したことを考えると、その先見の明に驚かざるを得ない。



ここで述べる料理のベースとして仕込んでストックしておくものは、実際、料
理の土台そのものであり、それなしでは美味しい料理を作ることの出来ない、
まず最初に必要なものだ。だからこそ、料理のベースとして仕込んでおくストッ
クはとても重要であり、いい仕事をしたいと努めている料理人ほどこれらを重
視している。


これらは、料理において常に立ち戻るべき出発点となるものだが[^115]、料理人がい
い仕事をしたいと望んでも、才能があっても、それだけでいいものを作ること
は出来ない。料理のベースを作るにも材料が必要なのだ。だから、必要な材料
は良質のものを自由に使えるようにしなければならない。

[^115]: 前段のいいかえだが、それだけエスコフィエが料理のベースとしてス
    トックしておくもの、すなわちフォンドキュイジーヌを重視していたこと
    の証左だろう。

筆者としては、むやみな贅沢には反対だが、それと同じくらい、食材コストを
抑え過ぎるのも良くないと考えている[^114b]。そんなことをしていては、伸びるはずの
才能の芽を摘んでしまうばかりか、意識の高い料理人ならモチベーションの維
持すら出来ないだろう。

[^114b]: エスコフィエのコスト意識を強調する文としてよく引用さ
    れるが、ほとんどの場合、後半を誤読していることに注意。「食材コスト
    を抑えるべき」と読んでしまっては、この後に続く文の意味がまったく異
    なり、支離滅裂になっててしまう。また、文脈を無視した引用はともすれ
    ば言葉がひとり歩きしてしまい、著者が本来意図した内容とまったく異なっ
    た意味で伝わることが多いので注意が必要。一例として、フランソワ・ラ
    ブレーの一節「ワインを飲んでいれば死ぬことはないでしょう」が有名だ
    ろう。これはパンタグリュエル出生の宴の席で、参列者が「やけに喉がか
    わく」といったのをうけて、別の参列者が、「ワインを飲んでいれば死ぬ
    こともなかろう=たくさん飲もう」という文脈で語られる言葉なのだが、
    現代日本ではどういうわけか、「ワインは健康にいい」という文脈にすり
    かえられて引用される言葉になってしまっている。


どんなに優秀な料理人だって、無から何かを作り出すことは不可能だ。期待さ
れる結果に対して、素材の質が劣っていたり量が足りないことがあれば、それ
でも料理人にいい仕事をしろと要求するなど言語道断である。


料理のベースとして仕込んでおくストックに関する重要ポイントは、必要な
材料は質、量ともに充分に、惜しげもなく使えるようにすることだ。


ある調理現場で可能なことが、別の調理現場では不可能な場合があるのはいう
までもない。料理人の仕事内容は顧客層によっても変わる。到達すべき目標に
よって手段も変わるということだ[^125]。

[^125]: 本書が料理の教科書であるにもかかわらず、さまざまな状
    況、顧客層を想定しているエスコフィエの慧眼にはおどろかされずにはい
    られないだろう。

そういう意味で、何事も相対的なものであるとはいえ、こと料理のベースとし
て仕込んでストックすべきものに関しては絶対に外してはならないポイントが
あるわけだ[^122]。組織のトップがこの点で出費を惜しんだり、コスト面で過度に目
くじらを立てるようでは、美味しい料理なんて出来るわけがないのだから、現
実に厨房を仕切っている料理長を批判する資格もない。そんなのが根拠のない
いいがかりなのは明らかだ。素材の質が悪かったり、量が足りないのであれば、
料理長が素晴しい料理を出せないのはいうまでもあるまい。ぶどうの搾りかす
に水を加えて醗酵させた安ワイン[^123]を立派な瓶に詰めてしまえば高級ワインにな
ると思うほどに馬鹿げたことはないのだ。

[^122]: 初版序文の「料理は進化し、新しいレシピが日々創案されている
    (p.ii)」および「カレームの時代にはこんにちと同じく既にあり、料理が
    続く限り、なくならないだろうものがある(p.iii」に呼応している。逆に
    いえば、解釈にもよるが、エスコフィエが fonds de cuisine フォンドキュ
    イジーヌに絶対的な信頼と信念を持っていたことがわかる。

[^123]: 原文 piquette （ピケット）。ここでは説明的に意訳したが、この種
    の偽物銘醸ワインの問題、スキャンダルは現代にも尾をひいている。これ
    は、人間が味覚、嗅覚そのもの以外の「情報」をも利用して「味わう」と
    いう行為をおこなっているがゆえのある種の錯覚を利用しているために判
    明しにくい側面がある。いっぽうで、中世のファブリオー（中世の艶笑潭
    を含む短編物語。約 300 近い物語が現存している。チョーサーの『カン
    タベリー物語』に少なくない数が英訳されて収録されている）には、
    3 人の婦人がタヴェルヌ（酒屋）で産地ごとの銘酒のテイスティングをお
    こなう物語があり、中世からワインの産地ごとによる格付けが一般的だっ
    ただろうことがわかる。なお、ピケットゥをはじめとした安価なワインは、
    マスタードのように、瓶詰めではなく樽詰めで瓶などの容器を客が持参し、
    小分けして販売されていたという。

料理人は、必要なものを何でも使っていいなら、料理のベースとして仕込んで
おくストックにとりわけ力を入れるべきであり、文句のつけようのない出来に
なるよう気を使うべきだ。そこに手間｜隙《ひま》かけていればそれだけ厨房全体の仕事
がきちんと進むのだから、注文を受けた料理をきちんと作れるかどうかは、結
局のところ、料理のベースとなる仕込み類にどれだけ手間隙をかけ
るかということなのだ[^124]。

[^124]: 本書（エスコフィエ）特有の畳語的、反復的表現。とりわけ原注にお
    いては「聞き書き」と思われるものも多く、反復的表現が目立つ。原書に
    おいてわかりにくい表現はしばしば別の表現で再度くりかえし、ただし挿
    入句的に書かれる傾向がある。


### 主要なフォンとストック[^102]  {#principaux-fonds-de-cuisine}

[^102]: この一覧は第二版から。この時点でソースの章の一項目として Les
    fonds de cuisine をまとめてしまったため、フォンドキュイジーヌとい
    う語が本来意図されたであろう意味とは異なり「出汁」つまりはいわゆる
    フォンに限定される誤解を招いたともいえよう。

料理のベースとして仕込んでおくべきものは主として……[^bis02]

- [標準的なコンソメ](#consomme-blanc-simple)と[クラリフィエしたコンソメ](#consomme-ordinaire)[^consomme-simple]

- [茶色いフォン](#fonds-brun-ou-estouffade)、[白いフォ
ン](#fonds-blanc-ordinaire)、[鶏のフォン](#fonds-de-volaille)、[ジビエ
のフォン](#fonds-de-gibier)、[魚のフォ
ン](#fonds-ou-fumet-de-poisson)……[とろみを付けた
ジュ](#jus-de-veau-lie)[^134bis]、[基本ソース](#grandes-sauces-de-base)の
ベースになる

- [フュメ](#fonds-ou-fumet-de-poisson)、[エッセンス](#essences-diverses)……派生ソースに用いる

- [グラスドヴィヨンド](#glace-de-viande)、[鶏のグラス](#glace-de-volaille)、[ジビエのグラス](#glace-de-gibier)

- [茶色いルー](#roux-brun)、[ブロンドのルー](#roux-blond)、[白いルー](#roux-blanc)

-  基本ソース……[エスパニョル](#sauce-espagnole)、[ヴル
テ](#veloute-ou-sauce-blanche-grasse)、[ベシャメル](#sauce-bechamel)、
[トマト](#sauce-tomate)。

- [肉料理用ジュレ](#gelees-grasses-ordinaires)、小斎[^134]のための
[ジュレ](#gelee-de-poisson-blanche)

[^consomme-simple]: 原文はそれぞれ consommés ordinaires （コンソメ・オ
    ルディネール）consommés clarifiés （コンソメ・クラリフィエ）である
    が、原書では同一表記であってもたびたび用語に混乱があることに注意。
    ここでは本書においてポタージュの章の、[白いコンソメ・サンプ
    ル](#consomme-blanc-simple)、[標準的なコンソ
    メ](#consomme-ordinaire)に相当する。

[^134]: （しょうさい）maigre （メーグル）ここではキリスト教の宗教用語。フラン
ス語 maigre はもともと「脂気のない、痩せた」という意味の形容詞。ところ
がこの語には宗教的な「小斉の」という意味もあり、しばしば日本の料理書や
調理現場ににおいて誤読、誤訳されてきた。さて、古代〜中世以来カトリック
では四旬節 carême （カレーム、19 世紀にこの名を持つ大料理人がいた）すな
わち復活祭の 46 日前は日曜を除き、基本的に断食の一種として獣肉を食べない
習慣があった。正確にはカトリックの「教義」ではなくあくまでも「習慣」と
されている。ここにもうひとつの断食の形態である「大斉」（たいさい）があ
る。小斉はとりわけ古代から中世にかけて四旬節の期間いっぱい毎日おこなわ
れ、その習慣は 19 世紀初頭まで守られつづけた。その後も敬虔な信仰を持つ信
者は小斉、大斉の習慣を守りつづけているという。小斉は日本の「お精進」に
似ているが、魚類、野菜類のほか乳製品、卵、チーズなども許容された。中世
から 18 世紀にかけて上流階級の家に仕える料理人にとって、この期間の料理の
献立が大問題であり、工夫をこらしたレシピが多くのこされている。エスコフィ
エの時代には魚料理の次に肉料理を供する習慣がはじまった頃だが、それでも
敬虔な信者が小斉の献立を求めることもあったようで、小斉の献立に配慮する
レシピ、記述が散見される。なお、復活祭の 46 日前を「灰の水曜日」といい、
その日は大斉と小斉をおこなう。さらにその前日までカーニバルというお祭り
がおこなわれるが、これは carne + vale 「肉よさらば」という意味だ。無宗
教の立場から考えると、冬至の頃に豚を屠畜して肉は塩漬けなどに、血や内臓
はソーセージなどの加工肉にしていたのを食べ尽すタイミングとおおむね一致
すると考える。1 年中時をかまわず家畜を屠畜している余裕がなかった時代に
はじまった習慣だろう。そして春になり、家畜が出産をはじめる。飼育しきれ
ない仔牛、仔羊、仔豚などは食材となる。このため復活祭以降は肉食が許され
た、と考えることも可能だろう。もちろん四旬節の 40 日という数字（日曜は主
日といい数に含めない）が、イエス・キリストが 40 日間の断食をおこなったこ
とに由来するというのがカトリックにおける理解だ。また、小斉、大斉な上述
のように「習慣」ではあるが、1962 年から 1966 年にかけておこなわれた第二バ
チカン公会議においても議論され、1966 年以降は灰の水曜日と聖金曜日（復活
祭の直前の金曜日）にのみ大斉とともに実施され、獣肉を摂らない、とされて
いる。もっとも古くは四旬節以外の季節も小斉が週に 1、2 度おこなわれ、それ
は金曜であることが多かった。

[^134bis]: 「とろみを付けた」は用語[リエ\*](#lier-gls) lier の訳語。ソースお
    よびポタージュの料理名ではしばしば日本語化して訳している。本文中で
    はリエまたはリエゾンのカナ書きで訳していることが多いので留意されたい。


以下も日常的に使う料理のベースとして仕込んでおくものとしてあつかう。

- [ミルポワ](#mirepoix)、[マティニョン](#matignon)

- [クールブイヨン](#court-bouillon-a)、[肉用のブロン](#les-blanchissages)、[野菜用のブロン](#blanchissage)

- [マリナード](#marinades-et-saumures)、[ソミュール](#saumures)

- 肉料理用ファルス、小斎用[ファルス](#farces)

- [ガルニチュールに用いるアパレイユ](#series-des-appareiles-et-preparations-diverses-pour-garnitures-chaudes)など……


本書は上記を順に説明していく構成にはなっていない。グリル、ロースト、グ
ラタン等の調理技法についても順を追っていくわけではない。料理の種類ごと
に一定の位置、つまりは関連の深い料理の章の冒頭において説明していくこと
になる。


そのようなわけで、本書においては以下のようになる……


- フォン、フュメ、エッセンス、グラス、マリナード、ジュレの説明……I.[^bis03] [ソース](#sauces)

- コンソメおよびそのクラリフィエ、ポタージュの浮き実についての説
  明……III. [ポタージュ](#potages)

- ファルスとガルニチュール用[アパレイユ\*](#appareil-gls)の作り方……II. [ガルニチュー
  ル](#garnitures)

- クールブイヨン、魚料理のファルス等……VI. [魚料理](#poissons)

- グリル、ブレゼ、ポワレの調理理論……VII. [ルルヴェとオントレ](#releves-et-entrees)


[^bis02]: ここで列挙されている概念、用語などは読み進めるうちに出
てくるので、そのつど理解に努め、覚えるようにするといいだろう。小斎につ
いては巻末の基本用語集参照のこと。



[^bis03]: 原書では章番号に**ローマ数字**が用いられており、本書も
それにならっている。ヨーロッパ文化に携わる者としては最低限の常識なので覚
えておきたい。I = 1, II = 2, III = 3, IV = 4, V = 5, VI = 6, VII = 7, VIII = 8, IX = 9,
X = 10, L = 50, C = 100, M = 1,000 が基本。ローマ数字であまり大きな数を扱う事
は少ないので 4 ケタが読めれば充分だろう。


<!--20200116江畑校正スミ-->

## 基本ソース  {#section-grandes-sauces-de-base}

<!--<div class="frsecenv">Grandes Sauces de Base</div>-->


\index{そす@ソース!きほん@基本---}
\index{sauce@sauce!grandes@Grandes ---s de Base}

* およびそれらを組み合せたり煮詰めるなどの方法で作るブラウン系、ホワイト系の派生ソース

* イギリス風ソース（温製および冷製)

* いろいろな冷製ソース

* ブール・コンポゼ（ミックスバター）

* マリナード

* ジュレ



### 概説 {#observation-sur-la-sauce}

ソースは料理においてもっとも主要な位置にある。フランス料理が世界に冠た
るもの[^113]なのもひとえにソースの存在によるのだ。だから、ソースは出来る
かぎり手間をかけ、細心の注意を払って作るようにしなければならない。

[^113]: フランス料理という概念と、それが他国より優れているという考え方
    はラヴァレーヌ『フランス料理の本』*Le Cuisinier François*,
    1651（この本の題名はしばしば『フランスの料理人』と訳されているが明
    らかに誤訳）の「出版者による序文」に表われている。「我らがフランス
    が文明の高さ、宮廷儀礼（クルトワジー）において世界中のほかの国より
    優れていることに加えて、誇り高き、上質な生活様式においても劣るとこ
    ろがまったくないからだ」という一節にも注目したい。料理書の序文であ
    るから、ここでの「生活様式」に料理あるいは食文化の意味が含まれてい
    ることはいうまでもないだろう。


ソースを作るうえでその基礎となるのが何らかの「ジュ」である[^03]。
すなわち、茶色いソースは「茶色いジュ」（エストゥファード）から作る。ヴ
ルテには「澄んだジュ」（白いフォン[^04]）を使う。ソースを担当する料
理人はまず第一に、完璧なジュを作るところから始めなければならない。キュ
スィ侯爵[^05]がいうように、ソース担当の料理人は「頭脳明晰な化学者
[^028]でありかつ天才的なクリエイターで、卓越した料理という建造物の
いわば大黒柱たる存在」なのだ。


[^03]: ここではジュといわゆる「出汁」の意味でのフォンが同じ意味で
使われている。ジュとフォン（ブイヨン）の区別があまり明確でない例として
は、ラヴァレーヌ『フランス料理の本』の冒頭におけるブイヨンの説明において「ローストした肉からはジュがとれる。
これを[ブーケガルニ\*](#bouquet-garni-gls)とともに加熱し、よく火を通したらオントレやブラウン色
のポタージュに使う (p.2)」という記述がある。


[^04]: 日本の調理現場で「白いフォン」を意味する「フォン・ブロン」は主と
    して鶏のフォンを指して用いられることが多いが、本書で扱われている白いフォンのう
    ち標準的なものは仔牛肉、家禽類をベースとしており、鶏のフォンは別途
    説明されている。


[^05]: 1767〜1841。19 世紀の著名な美食家。 著書に『食卓のクラシック』(1843)があ
    る。料理名にキュスィの名を冠したものも多い。


昔のフランス料理[^06]では、素材に串を刺してあぶり焼きするロースト
を別にすれば、どんな料理も「ブレゼ」か「[エチュヴェ\*](#etuver-gls)」のようなものばかり
だった。だが、その時代には既に、フォン[^116]が料理という大建築の丸天井の
｜要《かなめ》だったし、材料コストが重視されるこんにちの我々と比べたら想像
も出来ないくらい贅沢に材料を使ってフォンをとっていたのだ。実際、アンヌ・
ドートリッシュ[^07]がスペインからルイ十三世に嫁いだ際に随行してきた
スペインの料理人たちによってフランス料理にルー[^118]を用いる方法が伝えられた
が[^027]、当時はほとんど無視された。ジュそれだけで充分だったからだ。
ところが時代が下り、料理におけるコストの問題が重視されるようになった[^117]。
ジュはその結果、貧相なものになってしまった。その美味しさを補うものとし
て、ルーを用いて作る[ソース・エスパニョル](#sauce-espagnole)が欠くべからざる存在となった[^119]。


[^06]: 本書において「昔の料理」と表現される場合は概ね 17 〜 18 世紀末
    と考えていいが、ここでは中世以来 18 世紀までと理解していいだろう。と
    はいえ、この部分についてのみいえば、おもに中世から 17 世紀末について
    見事にあてはまる。中世以来のポタージュの多くがまさしく「ブレゼか[エ
    チュヴェ\*](#etuver-gls)のようなもの」だったのは事実であり、液体料理としてのポター
    ジュはむしろ少数だった。


[^07]: 17世紀に絶対王政を確立したルイ十四世の母。

[^116]: 19世紀前半まで、「出汁（だし）」の意味ではフォンではなくブイヨンと呼ばれた。

[^118]: ルー roux という語は本来、赤茶色、が原義。つまりブロンドのルー
    や白いルーという表現は矛盾もはなはだしいし、そもそも茶色い
    ルーという表現そのものが畳語になっている。とはいえ、17 世紀におい
    ては小麦粉を赤茶色になるまで炒めるという意味でルーの語が用いられて
    いた。

[^027]: ルーがスペインからもたらされたというのは逸話、伝承の域を出ない。

[^117]: 少なくともカレームの時代にはコストの問題は表面化していたよう
    だ。たとえばポワル（ミルポワと似ているが、バイヨンヌのハ
    ムや仔牛肉、各種香味野菜と香草をふんだんに使う）について、「時代が
    変わるにつれその時の料理人には、上で詳細に説明したようにポワルを用
    意する、などという表現が奇妙なものに見えるかも知れない。宴の主宰者
    は出費を許してはくれないだろう。だから、よりコストのかからない、け
    れども我々が慣れ親しんでいる調理原則とはもはや調和のとれない方法を
    とることになるだろう(III, p.47)」と記している。


[^119]: こんにちではほとんど作られないソースなのでイメージし難いかも知
    れないが、いわゆる欧風カレーライスがカレー粉入りのルーを用いたソー
    ス・エスパニョルの変形であると考えてもいいだろう。あるいはゼラチン
    質が多く、常温ではジュレ状になっているフォンドヴォーに相当するとい
    う考え方もあるだろう。[ソース・エスパニョル](#sauce-espagnole)のレ
    シピにおいて、ことさら「澄んだ」仕上がりを目指すことが強調されてい
    る点にとくに注意したい。

ソース・エスパニョルはその完成度の高さゆえに成功をおさめたわけだ。だが、
すぐに当初の目的を越えた使い方をされるようになった。19 世紀末には本当に
このソースが必要な場合以外にも使われたわけだ。ソース・エスパニョルの｜濫用《らんよう》に
よって、どんな料理も固有の香りのない、全部の風味の混ざりあったのっ
ぺりとした調子のものばかりになってしまった。


ようやく近年になって、料理の風味がどれも同じようなものであることに批判
が集まってきて、その結果として激しい揺り戻しが起きたのだった。グランド
キュイジーヌ[^120]では、透き通ったような薄い色合いでしかも風味のしっかりした
仔牛のフォンが見直されつつある[^121]。そのようなわけで、ソース・エスパニョル
それ自体の重要性はだんだん減っていくだろうと思われる。

[^120]: grande cuisine （グロンドキュイズィーヌ）、haute cuisine （オー
    トキュイズィーヌ）とも。庶民的な日常の食事とは一線を画する高級料理
    を意味する。古くは宮廷や貴族、大ブルジョワの食卓、とりわけ宴席の料
    理を意味し、タレーランに代表される食卓外交の強力な武器となった。の
    ちに政治体制が変わっても、富裕層のための料理であることに変わりはな
    く、それは現在もおなじといえよう。なお、庶民的な食事の代表はスープ
    であり、それは日本語で意味する液体料理とは異なり、固くなったパンを
    ブイヨンやワイン、オリーブオイルなどに浸して食べるもののこと。スー
    プに肉のブイヨンが用いられることが増えたのはもっぱら 19 世紀以降、つ
    まり産業革命以後となる。

[^121]: エスコフィエのこの予言はある意味で正鵠を射ていた。フォンドヴォー
    が多用された時代を経て、ベルナール・ロワゾーの「水の料理」に代表さ
    れるフォンを使わない時代を経過し、現代ではソースをともなわない料理やまった
    く新しい形態のソースなどが普及しつつある。

ソース・エスパニョルが基本ソースとして扱われるべき理由は何か？　ソース・
エスパニョルそれ自体に固有の色合いや風味というものはなく、これらはどん
なフォンを用いて作るかで決まる。まさにこの点にソース・エスパニョルの長
所が存するのだ[^126]。補助材料としてルーを加えるが、ルーには[リエゾン\*](#lier-gls)と
いう意味しかなく、風味にはまったく寄与しない。そもそも、ソースを完璧に
仕上げるためには、リエゾン以外のルーに含まれる成分はソースからほぼ完全に
取り除いてしまっても差し支えはない。丁寧にデプイエ[^133]したソースにはルーに含まれていたでんぷん質だけが残っ
ているわけだ[^127]。だから、ソースの口あたりをなめらかなものにするために必要な
のがでんぷん質だけなら、純粋なでんぷんだけを用いる方がずっと簡単で、作
業時間も大幅に短縮されるし、その結果として、ソースを火にかけすぎてしま
うようなミスも防げる。将来的には、小麦粉ではなく純粋なでんぷんやアロールート[^allow-root]でルーを
作るようになるかも知れない[^130]。


[^allow-root]: クズウコンから作られる良質なデンプン。日本ではコーンスターチで代用されることが多い。

[^126]: ややわかりにくい記述だが、ソース・エスパニョルを概念
    としてとらえるとわかりやすいだろう。つまり、主材料であるフォンに何
    を使うかによって、ソース・エスパニョルのバリエーションができるということ。

[^127]: この記述は正確ではない。小麦粉をバター（あるいは獣脂）で炒めて
    ルーを作っているために、多少なりとも油脂とフォンが乳化作用を起こし
    ていると考えられる。<!--とろみを付けたジュでは、純粋にでんぷんだけを用
    いてとろみを付けているのがその証左となるだろう。-->


[^130]: でんぷんのみを用いて、ソースの[リエゾン\*](#lier-gls)をおこなうのは、アピキ
    ウスまで遡ることができる。アピキウスでは米のでんぷんが使われていた。
    また、日本の初期のカレーライス（明治時代）では、ルーではなく最後に
    水溶き片栗粉を加えてリエしていた（cf. 仮名垣魯文『西洋料理
    通』）。とはいえ、でんぷんだけを用いたルーが実用化されることはなかっ
    たようだ。これは上述の、油脂とフォンなどの液体との乳化作用をもたら
    す小麦粉のルーの性質を再現できなかったためだろう。

[^133]: dépouiller（[デプイエ\*](#depouiller-gls)）。こんにちの日本の
調理現場ではほぼ[エキュメ\*](#ecumer-gls) (écumer)と区別されなくなっている用語。ソース
や煮込み料理を仕上げる際に、浮き上がってくる不純物を徹底的に取り除き、
目の細かい布などで漉すこと。原義は動物などの皮を剥ぐ、剥くことの意で、
野うさぎの皮を剥ぐ、うなぎの皮を剥く、という意味では現代の厨房でも用い
られている。ソースの場合は表面に凝固した蛋白質や油脂の膜が出来、それ
を「剥ぐように」取り除くことから、あるいは表面に浮いてくる不純物を徹底
的に取り除いてきれいなソースに仕上げることを、動物の皮を剥いてきれいな
身だけにすることになぞらえて、この用語が用いられるようになったようだ。
なお、本書においてécumer（エキュメ）が単に浮いてくる泡やアクを取る、と
いう作業であるのに対して、dépouiller（デプイエ）は「徹底的に不純物を取
り除いて美しく仕上げる」という意味合いが込められている。現代では品種改
良や農法の変化によって野菜のアクも少なくなり、小麦粉も精製度の高いもの
を利用出来るなど、食材および調味料の多くで純度の高いものを使用する場合
がほとんどであり、このデプイエという作業は 20 世紀後半にはほとんど行なわ
れなくなり、écumer（エキュメ）という用語だけで済ませることがほとんど
（cf.辻静雄監訳『オリヴェ ソースの本』柴田書店、1970年、27〜28頁）。


料理界の現状を｜鑑《かんが》みるに、[ソース・エスパニョ
ル](#sauce-espagnole)と[とろみを付けたジュ](#jus-de-veau-lie)をそれぞ
れ使い分けざるを得ない。これにはさまざまな理由があるが、大きな仕立ての
ブレゼや、羊や仔羊以外を材料にしたラグーでは、肉汁が煮汁に染み出してき
て美味しくなるわけだから、トマトを加えたソース・エスパニョルを用いるの
がいい。なお、ソース・エスパニョルをさらに丁寧に仕上げるとソース・ドゥ
ミグラスとなる[^128]。これはいろいろなソテーに不可欠なもので、今後も変わるこ
とはないだろう。

[^128]: カレームらが[ソース・エスパニョル](#sauce-espagnole)をいかに澄んだ仕上がりにするか
    に腐心していたことを考えると、それをさらに純化させたドゥミグラスも
    澄んだ仕上がりを目指していることがわかるだろう。この点が、日本の洋
    食で多用されるデミグラス、ドミグラスとの大きな違いだろう。


一方、牛や羊、家禽を使った繊細で軽い仕立ての料理にはとろみを付けたジュ
の方が好まれる。[デグラセ\*](#deglacer-gls)の際に少量だけ、料理の主素材と同じものからとっ
たジュを用いる[^131]。

<!--[^129]: もっぱらソテーについて用いられる技法。ソテー鍋で焼いた肉から浸
    みだした肉汁が鍋底にねっとりとグラス状に付着しているのを、少量の液
    体を加えて溶かしだすこと。決して「焦げを取り除くことではない」。-->

[^131]: この表現から、「ジュとは香味野菜などを用いずにつくった出汁」の
    ように理解すべきではないだろう。本節冒頭にあるように、ここではフォンとジュ
    がほぼ同じ意味で用いられていることに注意。

こんにちのフランス料理においては、肉とソースの調和がとれているべきとい
う、まことに理に適った厳守すべき決まりがある。


だから、ジビエ料理にはジビエのフォンを用いるか、とりたてて際立った個性
を持たないフォンを用いて作ったソースを添える。牛や羊のフォンは用いない[^135]。
ジビエのフォンというのは、さほど濃厚なものを作ることは出来ないが、素材
の個性的な風味を表現するには最適だ。こういった事情は魚料理にも当て
はまる。ソースそれ自体が際だった風味を持たないものの場合に
は必ず魚のフュメを加えてやるのだ。このようにしてそれぞれの料理に個性的
な風味を実現させることになる。

[^135]: 料理の素材と異なった個性を持ったソースを合わせようとすると不調
    和になるため。おなじ素材をベースにしたソースか、とりたてて個性的な
    風味とはいえない標準的なソース・エスパニョルなどを用いるべき、とい
    うこと。

もちろん、ここまで述べた原則を実現しようにも、コストの問題がしばしば起
こることは承知している。けれども、熱意のある、他者の評価を意識している
料理人[^132]なら問題点を熟考して、完璧とはいわぬまでも満足のいく結果を得るこ
とが出来るだろう。

[^132]: プロの料理人のつくる料理の評価とは、自己評価では決まらない。つ
    ねに食べ手による評価、つまり他者による評価に依存することになる。自
    己評価ではたんなる自己満足しか得られないのだ。いわば対他的
    存在であることが料理人という職業に運命づけられたものといえよう。



<!--20200117江畑校正スミ-->

### ソースのベース作り  {#traitement-des-elements-de-base}

\index{そす@ソース!そすつくりのへす@---のベース作り}
\index{sauce@sauce!Traitement des elements de base dans le travail des sauces@Traitement des Éléments de Base dans le Travail des ---s}

<div class="recette"><!--beginRecette-->

#### 茶色いフォン、エストゥファード[^136] {#fonds-brun-ou-estouffade}

\index{ふおん@フォン!ちやいろいふおん@茶色い---}
\index{えすとうふあと@エストゥファード}
\index{fonds@fonds!brun@--- brun}
\index{fonds@fonds!estouffade@estouffade (fonds brun)}
\index{estouffade@estouffade!fonds brun@ --- (fonds brun)}


<div class="frsubenv">Fonds brun ou Estouffade</div>

* 仕上がり 10 L 分

主素材……肉の付いたままの牛すね 6 kg、仔牛のすね 6 kgまたは仔牛の端肉で脂身を含
まないもの 6 kg、骨付きハムのすねの部分 1 本（事前に[ブロンシール
\*](#blanchir-gls)しておく）、塩漬けしていないブロンシールした豚皮 650 g。


香味素材……にんじん 650 g、玉ねぎ 650 g、[ブーケガルニ\*](#bouquet-garni-gls)（パセリの枝 100 g、
タイム 10 g、ローリエ 5 g、にんにく 1 片）。

作業手順……肉を[デゾセ\*](#desosser-gls)する。

骨は細かく砕き、オーブンに入れて軽く焼き色を付ける。野菜は[ルヴニール\*](#revenir-gls)する。
これらを鍋に入れて 14 Lの水を注ぎ、ゆっくりと、最低 12 時
間加熱する。水位が下がらぬように、適宜沸騰した湯を足すこと。


大きめの[デ\*](#des-gls)に切った牛すね肉を別鍋でルヴニールする。
先にとっておいたフォンを少量加えて煮詰める[^107]。この作業を 2 〜 3 回行な
い、フォンの残りを注ぐ。

鍋を沸騰させて、[エキュメ\*](#ecumer-gls)する。丁寧に[デグレセ
\*](#degraisser-gls)する。[エチュヴェ\*](#etuver-gls)にして完全に火を通したら、布で
｜漉《こ》してストックする。

[^136]: ここでは茶色いフォンの別名として出てきているが、肉や魚を蓋をし
    て[エチュヴェ\*](#etuver-gls)にした料理のこともエストゥファードと呼ぶ。

<div class="nota">（原注）フォンの材料に牛の骨などが含まれている場合には、事前にその骨だけで 12 〜 15
時間かけてとろ火でフォンをとるといい。</div><!--endNota-->


フォンの材料を鍋に焦げ付くくらいまで強く焼き色を付ける[^08]のはよろしくない。経
験からいって、丁度いい色合いのフォンに仕上げるには、肉に含まれているオスマゾーム
[^09]の働きだけで充分だ。

[^107]: この作業は faire tomber à glace （フェールトンベアグラス）とい
    い、18 世紀から 20 世紀前半までさかんに行われていた。なお 21 世紀でもこ
    の手法をもちいて「ジュ」をとるケースもある。その場合、パンセと混同
    されていることもあるので注意。

[^08]: パンセ pincer と呼ばれる手法。原義は「抓（つま）む」。材料
    が鍋底に張り付いて、トングなどでしっかり「抓ま」ないと取れないくら
    い強く焼き付けることからそう呼ばれるようになった。古い料理書では推
    奨するものも多かった。なお、19 世紀初頭の文献には pincer légèrement
    （パンセレジェールモン、軽くパンセする）といういいまわしがよく見ら
    れるが、これは[リソレ\*](#rissoler-gls)と同義なので注意。


[^09]: osmazome 19世紀頃、赤身肉の美味しさの本質であると考えられ
    ていた想像上の物質。赤褐色をした窒素化合物の一種で水に溶ける性質が
    あるとされた。なお、当時のヨーロッパではグルタミン酸やグアニル酸は
    もとよりイノシン酸が「うま味」の要素であるという概念すらなく、「コ
    クがある」corsé （コルセ）とか「肉汁たっぷり」onctueux （オンク
    チュー）やsucculent （スュキロン）などの表現で肉料理やソースの美味
    しさが表現されたが、アミノ酸による「美味しさ」そのものはよく知られ
    ていたこともまた事実であり、それを「うま味」という概念で認識してい
    なかっただけのこと。けっしてフランスの食文化において「うま味」がな
    かったわけではない。むしろ逆に、いかにイノシン酸を肉類から抽出する
    かを経験的に追究してきたといえる。19 世紀当時に化学者や料理人が空想
    し追究しようとしたオスマゾームの正体は現代でいうペプトン（たんぱく
    加水分解物の一種）だったという説もある。ところで、茶色いフォンの色
    については、肉や野菜、骨を焼き色が付くまで焼いたことによるところが
    大きい。つまりはメイラード反応発見したのがフランス人、
    ルイ・カミーユ・マイヤールなので「マイヤール反応」と呼びたいところ）
    の結果ととらえることができる。もしこれらの素材にまったく焼き色を付
    けなければ、いわゆるポトフと同様にごく薄い淡褐色になるのがせいぜい
    だろう。







\atoaki{}

#### 白いフォン  {#fonds-blanc-ordinaire}

\index{ふおん@フォン!しろい@白い---}
\index{fonds@fonds!blanc ordinaire@--- blanc ordinaire}

<div class="frsubenv">Fonds blanc ordinaire</div>


* 仕上がり 10 L 分


主素材……仔牛のすね、および端肉 10 kg、鶏の手羽やとさか、足など、ま
たは鶏がら 4 羽分[^137]。


香味素材……にんじん 800 g、玉ねぎ 400 g、ポワロー 300 g、セロリ 100 g、[ブー
ケガルニ\*](#bouquet-garni-gls)（パセリの枝 100 g、タイム 1 枝、ローリエの葉 1 枚、クローブ 4 本）。


使用する液体と調味料……水 12 L、塩 60 g。


作業手順……肉は骨を外し、紐で縛る。骨は細かく砕く。鍋に肉と骨を入
れ、水を注ぎ塩を加える。火にかけ、[エキュメ\*](#ecumer-gls)して香味素材を加
える[^140]。


加熱時間……弱火で 3 時間。


<div class="nota">（原注）このフォンは火加減を抑えて、出来るだけ澄んだ仕上がりにすること。丁寧に
[エキュメ\*](#ecumer-gls)し[デグレセ\*](#degraisser-gls)すること。</div><!--endNota-->


茶色いフォンの場合と同様に、始めに細かく砕いた骨だけを煮てから指定量の
水を注ぎ、弱火で 5 時間加熱する方法もある。


この骨を加熱した汁で肉に火を通すわけだ。その作業内容は上記茶色いフォ
ンの場合と同様。この方法は、骨からゼラチン質を完全に抽出出来るという利
点がある。当然のことだが、煮ている間に蒸発して失なわれてしまった分は湯
を足してやり、全体量を 12 Lにしてから肉を煮ること[^103]。


[^103]: 初版の原注は「鶏料理に添えるソースを作る場合には、このフォンで
    鶏を[ポシェ\*](#pocher-gls)すること。そうすればずっと美味しくなる」
    と記されているのみ。この記述から、第二版以降には「鶏のフォン」の項
    目が独立することになる。

[^137]: ここでは主素材に成牛の肉や骨、すなわちオスマゾームを含むと考え
    られていた素材を用いていないことに注意。主素材と香味素材に焼き色を
    付けず、弱火でやや短時間で仕上げているのも、透明なフォンをとるうえ
    でのポイント。

[^140]: 茶色いフォンの場合と異なり、tomber à glace （トンベアグラス）
    に相当する作業をおこなわないことに注意。

\atoaki{}

#### 鶏のフォン、フォンドヴォライユ {#fonds-de-volaille}

\index{ふおん@フォン!とりのふおん@鶏の---}
\index{fonds@fonds!volaille@--- de volaille}
\index{とり@鶏!ふおん@鶏のフォン}
\index{かきん@家禽!とりのふおん@鶏のフォン}
\index{うおらいゆ@ヴォライユ!ふおんとうおらいゆ@フォンドヴォライユ}

<div class="frsubenv">Fonds de volaille</div>

[白いフォン](#fonds-blanc-ordinaire)と同じ主素材[^138]、香味素材、水の量で、さらに鶏のとさかや手羽[^143]、ガ
ラを適宜増量し、廃鶏 3 羽を加えて作る。


[^138]: つまり仔牛の骨とすね肉をベースに、鶏を多くするということ。純粋
    に鶏、鶏がらだけでフォンをとるわけではないことに注意。

[^143]: 原文 abatis （アバティ）、鶏の副生物、すなわち手羽先、足、とさ
    か、内臓などのこと。原書では t がひとつになっているが、正しくは
    abattis と綴る。日本では、よく abats （アバ）食用獣の副生物（内臓、
    もつ）と混同される傾向にあるが意味内容がまったく違うので間違えない
    ように注意。とりわけ、とさかと[ロニョン\*](#rognon-gls)はフランス
    料理において象徴的な意味をもつ食材であることは覚えておきたい（[コ
    ンソメ・ガリア風](#consomme-a-la-gauloise)訳注参照）。

\atoaki{}

#### 仔牛の茶色いフォン[^139]、仔牛の茶色いジュ  {#fonds-ou-jus-de-veau-brun}

\index{ふおん@フォン!こうしのちやいろい@仔牛の茶色い---}
\index{しゆ@ジュ!こうしのちやいろいしゆ@仔牛の茶色い---}
\index{fonds@fonds!fonds de veau brun@--- de veau brun}
\index{jus@jus!jus de veau brun@--- de veau brun}
\index{こうし@仔牛!こうしのちやいろいふおん@---の茶色いフォン(ジュ)}
\index{veau@veau!fonds brun@fonds ou jus de --- brun}

<div class="frsubenv">Fonds, ou Jus de veau brun</div>

* 仕上がり 10 L 分

主素材……[デゾセ\*](#desosser-gls)した仔牛のすね肉と肩肉（紐で縛っておく）6 kg[^144]、
細かく砕いた仔牛の骨 5 kg。


香味素材……にんじん 600 g、玉ねぎ 400 g、パセリの枝 100 g、ローリエの葉
2 枚、タイム 2 枝。


使用する液体……[白いフォン](#fonds-blanc-ordinaire)または水 12 L。水を用いる場合は 1 Lあたり 3 g の塩を加える。


作業手順……厚手の片手鍋または寸胴鍋の底に輪切りにしたにんじんと玉ねぎを敷きつめる。
その他の香味素材と、あらかじめオーブンで焼き色を付けておいた骨と肉を鍋に加える。


蓋をして弱火にし[シュエ\*](#suer-gls)する。フォンま
たは水少量を加え、煮詰める。この作業をさらに 1 〜 2 回おこなう[^141]。残りのフォ
ンまたは水を注ぎ、蓋をし、沸騰させる。[エキュメ\*](#ecumer-gls)する。微沸騰の状
態で 6 時間煮る。


布で漉し、ストックしておく。使用目的や必要に応じて、さらに煮詰めてから
ストックしてもいい。

[^139]: fond de veau （フォンドゥヴォ）、エスコフィエは fonds の綴りに
    こだわったようだが、現代では fond と綴るのが一般的。フォン（出汁）
    のなかではもっとも汎用性があり、用いられているフォンのひとつだろう。
    仔牛、とりわけ雄は去勢して肥育するか、繁殖用にする以外は経済動物と
    して使いみちがないためやや安価に流通する。そのため、この仔牛のフォ
    ンは[茶色いフォン](#fonds-brun-ou-estouffade)よりも低コストでつく
    れたのがポイント。本書序文に書かれたコストの問題に通じるものがある
    だろう。つまりエスコフィエ的には、コスト軽減のために仔牛のフォンを
    用いるのではなく、仔牛のフォンを必要とする料理だから用いるという理
    屈。だから[ソース・エスパニョル](#sauce-espagnole)では茶色いフォン
    を用いる指示になっている。また、オスマゾームが含まれていないにもか
    かわらず「茶色」を実現しているため、茶色いフォンの原注の記述と自己
    矛盾を起こしていることにも注意。作業のプロセスが茶色いフォンとほと
    んどおなじであることにも注目したい。素材と作業のプロセスについては
    後述。


[^141]: 原文 tomber à glace （トンベアグラス）。20 世紀末にはこの作
    業はほとんどおこなわれなくなっていたようだが、現場によってはソース
    のベースとするジュのシュック（suc 肉のうま味の汁）を濃縮する意味で
    おこなわれている。

[^144]: 21世紀の現代ではフォンをとる現場もあまりなくなったようだが、20 世紀末か
    ら 21 世紀初頭にかけては、仔牛肉は使わずに、牛すじを用いる現場も少なくなかっ
    たらしい。香味野菜のほかに、色付けをかねてトマトコンソントレを加えるケースも
    多かったようだ。また、仔牛の骨と牛すじなどの成牛の端肉だけでは酸味がたつこと
    もあるようで、その場合は鶏を加えるなどして工夫していたらしい。いずれにしても、
    本書のレシピどおりの作り方をするケースはかなり珍しかったと想像される。20 世
    紀末ごろのあるホテルの厨房では、フォンドヴォーは次のようにしてとっていたとい
    う。まず砕いた仔牛の骨と牛すじ肉をかなりしっかりと焼き、一晩かけて冷ます。香
    味野菜を良く炒めて香りをしっかり引き出し、トマトコンソントレを加えて骨と肉を
    加えて水を注ぎ弱火でゆっくり加熱していた。これは一例であり、時代や厨房現場に
    よって差異があることは充分に考えられる。また、日本語で「2 番のフォン」という
    表現があるが、これは文字どおりフォンをとった「だしがら」に水を加えて再度「出
    汁」をとったもので、煮詰めて煮込みなどに用いられていたという。このように、か
    なり食材コストをおさえたもので、本書のレシピなどはかなり贅沢に材料を用いてい
    るが、エスコフィエの「費用対効果」の意識と、フォンドキュイジーヌ（あらかじめ
    仕込んでストックしておくもの）にはいい材料をふんだんに使うべき、という考え方
    が結果的に共存していたようにも見える。そういう意味では、20 世紀末から 21 世
    紀初頭の現場で作られるフォンドヴォーは茶色いフォンと、この仔牛の茶色いフォン
    の折衷的なものだったのかも知れない。また、21 世紀初頭ごろから、仔牛の骨など
    に含まれるゼラチン質が「のっぺりとした重さ」を感じさせ、軽やかなソースになら
    ないという論調があり、フォンにおけるゼラチン質の抽出を避ける料理人も少なくな
    い。

\atoaki{}

#### ジビエのフォン[^142]  {#fonds-de-gibier}

<div class="frsubenv">Fonds de gibier</div>

\index{ふおん@フォン!しひえ@ジビエの---}
\index{fonds@fonds!fonds de gibier@--- de gibier}
\index{しひえ@ジビエ!ふおん@---のフォン}
\index{gibier@gibier!fonds@fonds de ---}


* 仕上がり 5 L 分


主素材……シュヴルイユ[^109]の首肉、胸肉および端肉 3 kg（老い
たシュヴルイユがいいが、新鮮なものを使うこと）、[リエーヴル\*](#lievre-gls)の
端肉 1 kg、老うさぎ 2 羽、ペルドリ[^perdrix] 2 羽、老フゾン[^faisan] 1 羽。


香味素材……にんじん 250 g、玉ねぎ 250 g、セージ 1 枝、ジュニパーベリー[^011] 15 粒、
標準的な[ブーケガルニ\*](#bouquet-garni-gls)。


[^011]: セイヨウネズの樹の実。

[^perdrix]: ペルドロー perdreau の成鳥。孵化後 1 年以上。

[^faisan]: 雉のことだが、日本のものとは食材としてはかなり異なるためカナ書きしている。

使用する液体……水 6 Lおよび白ワイン 1 瓶。


加熱時間……3 時間。



作業手順……ジビエは事前にオーブンで焼き色を付けておき、野菜と香草を敷き詰めた鍋に
入れる。野菜類も事前に焼き色を付けておくこと。ジビエを焼くのに用いた天板を白ワイ
ンで[デグラセ\*](#deglacer-gls)し、これを鍋に注ぐ。同量の水も加え、ほぼ水分がな
くなるまで煮詰める。

この作業の後で、残りの水全量を注ぎ、沸騰させる。丁寧に[エキュメ\*](#ecumer-gls)しながら
ごく弱火で煮る[^012]。


[^109]: chevreuil 和名ノロ鹿。鹿類のなかではフランスでもっとも一般的食材。
    用語集[シュヴルイユ\*](#chevreuil-gls)参照。


[^012]: 最後に布で漉す必要があるが、当然のこととして明記されていないの
    で注意。

[^142]: 本書では fumet de gibier （フュメドゥジビエ）と表現されている
    ことも多い。フュメとは香気のことで、素材の香りを生かし、
    また強調するようにしてとったフォンのことをいう。

\atoaki{}

#### 魚のフォン、フュメドポワソン[^013]  {#fonds-ou-fumet-de-poisson}

\index{ふおん@フォン!さかな@魚の---}
\index{ふゆめ@フュメ!さかな@魚の---}
\index{ふゆめ@フュメ!ほわそん@フュメドポワソン}
\index{fumet@fumet!fumet de poisson@--- de poisson}
\index{fonds@fonds!fumet de poisson@fumet de poisson}

<div class="frsubenv">Fonds, ou Fumet de poisson[^104]</div>

[^104]: 初版では Fumet de poisson の名称のみで、fonds の語は使われていない。

* 仕上がり 10 L 分


主素材……舌びらめ、[メルロン\*](#merlan-gls)や[バルビュ\*](#barbue-gls)のアラ 10 kg。


香味素材……[エマンセ\*](#emincer-gls)した玉ねぎ 500 g、根パセリ[^016]の根と枝 100 g、
マッシュルームの切りくず[^016bis] 250 g、レモンの搾り汁 1 個分、粒こ
しょう 15 g（これはフュメを漉す 10 分前に投入する）。


使用する液体と調味料……水 10 L、白ワイン 1 瓶。液体 1 Lあたり 3 〜 4 g の
塩。


加熱時間……30 分。


作業手順……鍋底に香味野菜を敷き詰め、魚のアラを入れる。水と白ワイ
ンを注ぎ、強火にかける。丁寧に[エキュメ\*](#ecumer-gls)し、微沸騰の状態を保つようにする。
30 分煮たら目の細かい網で漉す。



<div class="nota">（原注）質の悪い白ワインを使うと灰色がかったフュメになってしまう。品質の疑わしい
ワインは使わないほうがいい。</div><!--endNota-->


このフュメはソースを作る際に加える液体として用いる。[小斎用ソース・エスパニョ
ル](#sauce-espagnole-maigre)を作ることを想定する場合には、魚のあらにバター
[^0101-bisbis02]を加え、[エチュヴェ\*](#etuver-gls)してから水と白ワインを注いで
加熱するといい。

[^0101-bisbis02]: バターは[小斎\*](#maigre-gls)においても使用が認められた食材だっ
    た。バターはフランス食文化史において、少なくとも中世以来長く用いられてきたが、
    中世〜ルネサンス時代は獣脂（もっぱらラード）のほうが料理に用いられる傾向にあっ
    た。17 世紀以降はたとえばラ・ヴァレーヌ『フランス料理の本』におけるアスパラガ
    スの白いソース添え（[ソース・オランデーズ](#sauce-hollandaise)訳注参照）のよ
    うに、バターを料理に用いることが中世の料理書と比較すると圧倒的に増えた。ムノ
    ンの 1741 年刊『ブルジョワ屋敷に勤める女性料理人のための本』のバターの項には
    「良質のバターを用いるのは料理でとても大事なことで、バターが悪い匂いを放って
    いてはどんな素晴しい皿も台無しだ。料理担当の女中であればそれをよく理解し、良
    質なバターを手に入れるのに金を惜しんではならないと肝に銘じておくこと。いいバ
    ターは自然な黄色で、白いのは大抵さして美味しくない。バルボットとかいう植物か
    ら採った黄色で着色したバターもある。こういうものは自然なバターの黄色よりもく
    すんでいるから、慣れれば簡単に見分けられる(p.320)」と書いている。ところで、
    現代フランスのバターは無塩のものと、ブルターニュ産に代表される有塩のものがあ
    り、料理および製菓では基本的に無塩バターを用いる。生乳をとる牛の品種や製法は
    さまざまだが、乳酸醗酵させたいわゆる醗酵バターが多い点が日本と大きく異なる。

[^013]: fumet de poisson （フュメドゥポワソン）。本質的には前出の「フォン」と同
    様のものだが、魚（およびジビエ）を素材としたフォンは香りがポイントとなるため、
    フュメ fumet（香気、良い香りの意）の名称が一般的に使われている。初版では魚の
    フュメとのみ記されていたのが第二版以降、魚のフォン、フュメ、と現行の表記になっ
    た。ある意味でフュメの役割から後退しているようにも見えるが、これを用いるレシ
    ピのほとんどは魚のフュメの表記のまま。

[^016]: 原文 racine de persil （ラスィーヌドゥペルスィ、文字通り
訳すとパセリの根）だが、実際には persil tubéreux （ペルスィチュベルー）
根パセリを用いる。根パセリはパースニップに似た見た目、つまり根がに
んじんのような円錐形で白い。葉はイタリアンパセリとよく似ていて平葉。も
ちろん葉も食用として使える。文字通り一般的なモスカールドのパセリの根を使うわけでは
ないので注意（モスカールドのパセリの根は固いだけで香りがなく、この用途
で使いものにならない）。

[^016bis]: champignons de Paris （ションピニョンドゥパリ）いわゆ
    るマッシュルームは、ガルニチュールなど料理の一部として提供する際に、
    カヌレ canneler、トゥルネ tourner といって螺旋（らせん）状の切れ込
    みを入れて装飾したものを使う。その際に少なくない量、具体的にはマッ
    シュルームの重量で 15 〜 20 ％ 程度が「切りくず」として発生するのでこれ
    を利用する。この場合だと、少なくとも 650 〜 750 g 程度のマッシュルーム
    の下処理（トゥルネ）をする必要があるが、大きな調理現場以外で毎日そ
    れほどのマッシュルームを消費するケースは少ないと思われるので、この
    レシピのとおりに作るには、切りくずを数日かけて冷蔵庫などで貯めてお
    くなどの工夫が必要だろう。本書のレシピ、とりわけフォンやフュメ、ソー
    スにおいてマッシュルームの切りくずを用いる指示が少なくないので留意
    されたい。なお、tourner（トゥルネ）の原義は「回す」であり、包丁を
    持った側の手は動かさずに、材料のほうを回すようにして切れ目を入れた
    り、アーティチョークや果物などの皮を剥くことを意味する。

\atoaki{}

#### 赤ワインを用いた魚のフォン[^145]  {#fonds-de-poisson-au-vin-rouge}

\index{ふおん@フォン!あかわいんをもちいたさかなのふおん@赤ワインを用いた魚の---}
\index{fonds@fonds!fonds de poisson au vin rouge@--- de poisson au vin rouge}

<div class="frsubenv">Fonds de poisson au vin rouge</div>

このフォンそれ自体を用意することは滅多にない。というのも、例えばマトロッ
トのような料理の魚の煮汁そのものだからだ。


とはいえ、こんにちでは魚のアラをすっかり[デバラセ\*](#debarrasser-gls)した状態で料理を提供する
必要がますます高まってきているので、ここでそのレシピを記しておくべきだ
ろう。このフォンの必要性と有用さはどんどん高まっていくと思われる。


原則として、このフォンの仕込みには、料理として提供するのと同じ種類の魚
のアラを用いて、その香りの特徴を生かす必要がある。だが、どんな種類の魚
を使う場合でも作り方は同じだ。


* 仕上がり 5 L 分

主素材……料理に用いるのと同じ魚種のアラ 2 $\frac{1}{2}$ kg。


香味素材……[エマンセ\*](#emincer-gls)して[ブロンシール\*](#blanchir-gls)した玉ねぎ 300 g、パセリの枝 100 g、タイ
ムの小枝 1 本、小さめのローリエの葉 2 枚、にんにく 5 片、マッシュルームの切りくず 100 g。


使用する液体と調味料……水 3 $\frac{1}{2}$ L、良質の赤ワイン 2 L、塩 15 g。


加熱時間……30 分。


作業手順……魚の白いフォン[^017]と同様にする。


<div class="nota">（原注）このフォンは魚の白いフォンよりも濃く煮詰めることが可能。とはいえ、保存の
ために煮詰めないでいいように、その都度、必要な量だけ仕込むことを勧める。</div><!--endNota-->


[^017]: 前項の[フュメドポワソン](#fonds-ou-fumet-de-poisson)のこと。

[^145]: このレシピは第二版から。「滅多にない」にもかかわらず掲載されているのは、
    「魚のアラをすっかり[デバラセ\*](#debarrasser-gls)した状態で料理を提供する必
    要がますます高まってきている」ためだが、21 世紀の現代において魚料理はほとんど
    が 1 人分の切り身の状態で調理、提供されるから、エスコフィエの慧眼（けいがん）
    にあらためて驚かずにはいられない。とはいえ、マトロットのような煮込み料理での
    使用を前提とするならやはりソースと一体化して煮込まれることが望ましいので、こ
    のフォンを用いてのオペレーションはシステマティックに過ぎるようにも思われる。




\atoaki{}
<!--20200119江畑校正確認スミ-->

#### 魚のエッセンス  {#essence-de-poisson}

\index{えつせんす@エッセンス!さかな@魚の---}
\index{essence@essence!poisson@--- de poisson}

<div class="frsubenv">Essence de poisson</div>

主素材……[メルロン\*](#merlan-gls)および舌びらめのアラ 2 kg。

香味素材……[エマンセ\*](#emincer-gls)した玉ねぎ 125 g、マッシュルームの切りくず 300 g、
パセリの枝 50 g、レモンの搾り汁 1 個分。

使用する液体……煮詰めていない[フュメドポワソン](#fonds-ou-fumet-de-poisson) 1 $\frac{1}{2}$ L、良質の白ワイ
ン 3 dL。

所要時間……45 分。

作業手順……鍋にバター 100 gと玉ねぎ、パセリの枝、マッシュルームの切りくずを入れ、
強火で色づかないよう[ルヴニール](#revenir-gls)する。アラと端肉を加える。蓋をして
約 15 分[エチュヴェ\*](#etuver-gls)する。その間、小まめに混ぜてやること。白ワイン
を注ぎ、半量になるまで煮詰める。最後にフュメドポワソンを注ぎ、レモン汁と塩 2 gを
加える。


再び火にかけて、とろ火で 15 分程加熱したら、布で｜漉《こ》す[^146]。

<div class="nota">（原注）魚のエッセンスは、[ソール\*](#sole-gls)や[チュルボ\*、チュルボタン\*](#turbot-turbotin-gls)、
[バルビュ\*](#barbue-gls)などのフィレ[^020]を[ポシェ\*](#pocher-gls)する際に用いる。</div><!--endNota-->

さらに、このエッセンスを煮詰めて、上記でポシェした魚のソースに加えて風
味を強くするのに使う。

[^020]: 魚の場合フィレは 3 枚おろし、または 5 枚おろしにして、ア
ラを取り除いた状態。

[^146]: エッセンスというとトリュフエッセンスやバニラエッセンスのように
    素材の香味成分を溶媒を用いて抽出したものを想起するが、本書でのエッ
    センスは事項「[エッセンスについて](#essences-diverses)」で定義されているように「ごく少量
    になるまで煮詰めて非常に強い風味を持たせたもの」と定義されている。
    マッシュルームエッセンスであれば、マッシュルームの切りくずあるいは
    マッシュルームそのものを煮詰めて香味成分を凝縮する。ともすればさら
    に事項の「グラス」と混同しやすいので、現代のエッセンスとの違いに注
    意したい。


\atoaki{}

#### エッセンスについて  {#essences-diverses}

\index{えつせんす@エッセンス!えつせんすについて@---について（フォン）}
\index{essence@essence!diverses@---s diverses (fonds)}

<div class="frsubenv">Essences diverses</div>



その名のとおり、エッセンスとはごく少量になるまで煮詰めて非常に強い風味
を持たせたフォンのこと。


エッセンスは普通のフォンと本質的には同じものだが、素材の風味をしっかり
出すために、使用する液体の量はずっと少ない。したがって、仕上げにエッセ
ンスを加える指示がある料理の場合でも、そもそも充分に風味ゆたかなフォン
を用いていれば、エッセンスは必要ないことが分かるだろう。


まず最初に、美味しく風味ゆたかなフォンを用いるほうが、あまり出来のよく
ないフォンで調理し、後からエッセンスで欠点を補うよりもずっと簡単なのだ。
その方がいい結果が得られるし、時間と材料の節約にもなる。


セロリ、マッシュルーム、モリーユ[^021]、トリュフなど、とりわけ明確な風
味の素材のエッセンスを、必要に応じて用いるにとどめるのがいい。


また、十中八九、フォンを仕込む際に素材そのものを加えた方が、エッセンス
を仕込むよりもいい結果が得られることは頭に入れておくこと。


そのようなわけで、エッセンスについてこれ以上長々と述べる必要もないと思
われる。ベースとなるフォンがコクと風味がゆたかなものであるなら、エッ
センスはまったく無用の長物といえる。


[^021]: morille キノコの一種。和名アミガサタケ。







\atoaki{}

#### グラス[^147]について  {#glaces-diverses}

<div class="frsubenv">Glaces diverses</div>

\index{くらす@グラス!くらすについて@---について}
\index{glace@glace!diverses@---s diverses}


[グラスドヴィアンド](#glace-de-viande)、[鶏のグラス](#glace-de-volaille)（グラスドヴォライユ）、[ジビエのグラス](#glace-de-gibier)、
[魚のグラス](#glace-de-poisson)の用途は多岐にわたる。これらは、上記いずれかの素材でとったフォ
ンをシロップ状になるまで煮詰めたもののことだ。


これらの使い途は、料理の仕上げに表面に塗ってしっとりとした艶を出させる
のに用いる場合もあれば、ソースの味や色合いを濃くするために用いたり、あ
るいは、あまりに出来のよくないフォンで作った料理の場合にはコクを与える
ために使うこともある。また、料理によっては適量のバターやクリームを加え
てグラスそのものをソースとして用いることもある[^148]。


グラスとエッセンスの違いだが、エッセンスが料理の風味そのものを強くする
ことだけが目的であるのに対して、グラスは素材の持つコクと風味をごく少量
にまで濃縮したものだ。


だからほとんどの場合、エッセンスよりもグラスを使うほうがいい。


とはいえ昔の料理長たちの中には、グラスの使用を絶対に認めない者もいた。
その理由は、料理を作る度に毎回その料理のためのフォンをとるべきであり、
それだけで料理として充分なものにすべき、ということだった。


確かに時間と費用の点で制限がなければその理屈は正しい。だが、こんにちで
は、そのようなことの出来る調理現場はほとんどない。そもそもグラスは、正
しく適量を用いるのであれば、そのグラスが丁寧に作られたものであるなら、
素晴しい結果が得られる。 だから多くの場合、グラスはまことに有用なもの
といえる。

[^147]: 原義は氷、鏡。よくできたグラスには艶があり、鏡のような仕上りを
    目指すことになる（ちなみに粉糖の意味で glace de sucre グラスドゥ
    スュークル、sucre glace スュークルグラスの表現はよく用いられる)。
    グラス、とりわけ[グラスドヴィアンド](#glace-de-viande)の歴史は比較的古く、ナポレオン軍
    の携行食料として採用されていたくらいだ。18 世紀、マラン『コモス神の
    贈り物』においても、コンソメの名称で事実上のグラスドヴィアンドのレ
    シピが収録されている。もっともこんにち考えるような、たんにフォンを
    煮詰めただけのものと違い、何度もフォンを煮詰めては新しい材料でフォ
    ンをとり、グラス状にして「完成」させるというもの（コンソメ
    consommé は「完成されたもの」、が原義）。また、19 世紀から 20 世紀初
    頭の料理書を原書で読む場合には faire (laisser) tomber à glace
    （フェール、レセ、トンベアグラス）という表現は覚えておくといい。
    「グラス状になるまで煮詰める」の意でよく用いられる。

[^148]: 本書では glace de viande dissoute （グラスドゥヴィヨンドゥディ
    スートゥ）溶かしたグラスドヴィアンド、という表現が頻出する。


\atoaki{}

#### グラス・ドゥ・ヴィヨンド  {#glace-de-viande}

<div class="frsubenv">Glace de viande</div>

\index{くらす@グラス!ういよんと@---・ドゥ・ヴィヨンド}
\index{glace@glace!viande@--- de viande}


[茶色いフォン（エストゥファード）](#fonds-brun-ou-estouffade)を煮詰めて作る。


煮詰めて濃くなっていく途中、何度か布で漉して、より小さな鍋に移しかえて
いく。煮詰めている際に、丁寧に[エキュメ\*](#ecumer-gls)することが、澄んだグラスを作るポイ
ント。


煮詰めている際には、フォンの濃縮具合に応じて、火加減を弱めていくこと。
最初は強火でいいが、作業の最後の方は弱火にしてゆっくり煮詰めてやること。


スプーンを入れてみて、引き上げた際に、艶のあるグラスの層でスプーンが覆
われ、しっかり張り付いているくらいが丁度いい。要するに、スプーンがグラ
スでコーティングされた状態になればいいということだ。


<div class="nota">（原注）色が薄くて軽い仕上がりのグラスが必要な場合には、茶色いフォンではなく、
[標準的な仔牛のフォン](#fonds-ou-jus-de-veau-brun)を用いる。</div><!--endNota-->


\atoaki{}

#### 鶏のグラス（グラス・ドゥ・ヴォライユ） {#glace-de-volaille}

<div class="frsubenv">Glace de volaille</div>

\index{くらす@グラス!とり@鶏の---（---ドヴォライユ）}
\index{くらす@グラス!うおらいゆ@---ドヴォライユ}
\index{glace@glace!volaille@--- de volaille}


[鶏のフォン](#fonds-de-volaille)（フォンドヴォライユ）を用いて、[グラスドヴィアンド](#glace-de-viande)と同様にし
て作る。





\atoaki{}

#### ジビエのグラス  {#glace-de-gibier}

<div class="frsubenv">Glace de gibier</div>

\index{くらす@グラス!しひえ@ジビエの---}
\index{glace@glace!glace de gibier@--- de gibier}
\index{しひえ@ジビエ!くらす@---のグラス}
\index{gibier@gibier!gibier@glace de ---}


[ジビエのフォン](#fonds-de-gibier)を煮詰めて作る。ある特定のジビエの風味を生かしたグラスを
作るには、そのジビエだけでとったフォンを用いること。





\atoaki{}

#### 魚のグラス {#glace-de-poisson}

<div class="frsubenv">Glace de poisson</div>


\index{くらす@グラス!さかな@魚の---}
\index{glace@glace!poisson@--- de poisson}



このグラスを用いることはあまり多くない。日常的な業務においては「[魚のエッ
センス](#essence-de-poisson)」を用いることが好まれる。そのほうが魚の風味も繊細になる。魚のエッ
センスで魚を[ポシェ\*](#pocher-gls)した後に煮詰めてソースに加える（魚のエッセンス参照）。






</div><!--endRecette-->



### ルー {#roux}

<!--<div class="frsecenv">Roux</div>-->

\index{る@ルー}
\index{roux@roux}


ルー[^149]はいろいろな派生ソースのベースとなる基本ソースの[リエゾン\*](#lier-gls)の役目
を持つ。ルーの仕込みは、一見したところさほど重要に思われぬだろうが、実
際には正反対だ。丁寧に注意深く作業すること。


[茶色いルー](#roux-brun)は加熱に時間がかかるので、大規模な調理現場では前もって仕込ん
でおく。[ブロンドのルー](#roux-blond)と[白いルー](#roux-blanc)はその都度用意すればいい。

[^149]: 既に述べたように（[本節概説](#osbservation-sur-la-sauce)訳注参
    照）、ルー roux という言葉の原義は赤茶色なので、以下に展開されるルー
    のバリエーションは畳語もしくは自己矛盾した表現になることに注意。そ
    もそもこの語が料理において使われるようになったのは17 世紀、ラヴァレー
    ヌ『フランス料理の本』（1651）である。料理名としては、赤茶色のアル
    エットのポタージュ Potage d'alouettes au roux (p.19) であり、この
    レシピでは、アルエットを[ブロンシール\*](#blanchir-gls)し、小麦粉
    をまぶしてフライパンで赤茶色になるまで焼く。これをブイヨンと[ブー
    ケガルニ\*](#bouquet-garni-gls)とともに鍋に入れて加熱し、パンを加えて
    弱火で煮込む。牛のパレ（軟口蓋）と羊のジュ、レモン果汁を添えて供す
    る、というもの(p.11)。素材に小麦粉をまぶして赤茶色になるまで焼いて
    から煮込めばソースはおのずと茶色になる。別の例としては、マクルーズ
    のポタージュ・ナヴェ添えがある。こちらは、マクルーズ（くろ鴨）を掃
    除したらうなぎか鯉の身を縦長の棒状に切ってラルデする。フライパンで
    焼いたら、水と、バターで[リエ\*](#lier-gls)したえんどう豆のピュレ
    同量ずつ、ブーケガルニを加えて加熱する。火がとおったらナヴェを切っ
    て小麦粉をまぶし、赤茶色になるまで炒める。マクルーズとともに弱火で
    煮込み、火が通ったらパンを加え、ナヴェとともに供する。リエゾンが足り
    ない場合は小麦粉を足し、ケイパーかヴィネガーを足す(p.170)。このよ
    うに、ソースのリエゾンの主役はあくまでも中世以来のパンではあるけ
    れども、小麦粉を素材にまぶして赤茶色にするという点が 17 世紀当時とし
    ては新しい技法だったといえる。それ以前にポタージュ（pot （ポ、鍋を
    使う料理全般））を茶色に仕上げるには、パンをよく焼いて色付けたもの
    をすり潰してワインやヴィネガーで溶いてから布でこし入れるのが一般的
    だった。リエゾンそのものについては、中世にはパンを上記のように粉
    にしたものを液体で溶いて布でこし入れるか、卵黄を用いるのがほとんど
    だった。これはパンに使われていた小麦粉の精白度合いとも関係があるか
    も知れない。リエゾンの方法で古くは、アピキウスで米のでんぷんを利
    用する方法が出ている（56 〜57）。さて、18 世紀にはまだルーを料理のパー
    ツとして用意するという概念はなかったようで、ムノン『宮廷の晩餐』
    （1755）では標準的なクリ（ソースのこと）において、肉をカラメル色に
    なるまで焼き、そこに小麦粉を加えてさらに赤茶色になるまで加熱する、
    とある(p.72)。ヴァンサン・ラシャペルにおいても、ポタージュのリエゾン
    付けにはパンを用いるのが一般的だった。マラン『コモス神の贈り物』
    （1758）においても、小麦粉と溶かしバターかラードを熱して色付けし、
    ブイヨンを注ぐ方法が出ている(p.10)。ところが 19 世紀初頭にはすでに、
    ヴィアール『帝国料理の本』（1806）においてブロンドのルーと白いルー
    のレシピが出ている。鍋に入れた小麦粉に溶かしバターを混ぜ込む方法だ
    が、ポイントはエスコフィエになっても変わっていないといえる。興味深
    いのは白いルーの使い方で、「ヴルテやその他のソースを[リエ
    \*](#lier-gls)するのに用いる(p.46)」とあること。1814 年のボヴィリエ
    『料理技術』にいたっては、ルーそのものと白いルーの項目が立てられて
    いる。カレーム『19 世紀フランス料理』では、ヴルテ用の白いルー、とエ
    スパニョル用のブロンドのルーが収録されており、ジュルドン・ルコント
    なる凡庸な医師兼料理人がルーを発明したかのように書かれている。ルー
    が基本ソースと派生ソースにとって、文筆家のインクであるようなものだ
    とも書いている(I, p.56)。





\atoaki{}



<div class="recette"><!--beginRecette-->


#### 茶色いルー  {#roux-brun}

\index{る@ルー!ちやいろ@茶色い---}
\index{roux@roux!brun@--- brun}

<div class="frsubenv">Roux brun</div>

* 仕上がり 1 kg 分

1° 澄ましバター……500 g

2° ふるった小麦粉……600 g


\atoaki{}

#### ルーの火入れについて {#cuisson-des-roux}

\index{る@ルー!ひいれについて@---の火入れについて}
\index{roux@roux!cuisson@cuisson du ---}

加熱時間は使用する熱源の強さで変わってくる。だから数字で何分とはいえな
い。ただし、火力が強過ぎるよりは弱いくらいの方がいい。というのも、温度
が高すぎると小麦粉の細胞が硬化して中身を閉じ込めてしまい、そうなると後
でフォンなどの液体を加えた際に上手く混ざらず、滑らかに[リエ\*](#lier-gls)したソースに
ならない。乾燥豆をいきなり熱湯で茹でるのと同じようなことが起きるわけだ。
低い温度から始めてだんだんと熱くしていけば、小麦粉の細胞壁がゆるんで細
胞中のでんぷんが膨張し、熱によって発酵状態の初期のようになる。このよう
にして、でんぷんをデキストリンに変化させる[^024]。デキストリンは水溶性
の物質で、これが「リエゾン」の主な要素なのだ。茶色いルーは淡褐色の美しい
色合いで滑らかな仕上がりにする。だまがあってはいけない。


ルーを作る際には必ず、澄ましバターを使うこと[^025]。 生のバターに
は相当量のカゼインが含まれている。カゼインがあると火を均質に通すことが
出来なくなってしまう。とはいえ、以下を覚えておくといい。ソースとして仕
上げた段階で、ルーで使ったバターは風味という点ではほとんど意味が失なわ
れている。そもそも[デプイエ](#depouiller-gls)する段階でバター
も完全に取り除かれてしまうわけだ。だからルーに用いるバターは小麦粉に熱
を通すためだけのものと考えていい。


ルーはソース作りの出発点だ。だから次の点も記憶に｜留《とど》めること。
小麦粉にでんぷんが含まれているからこそソースに「とろみ」が付く。だから
純粋なでんぷん（特性が小麦のでんぷんと同じでも異なったものでも）でルー
を作っても、小麦粉の場合と同様の結果が得られるだろう。ただしその場合は
小麦粉でルーを作る場合より注意して作業する必要がある。また、小麦粉と違っ
て余計な物質が含まれていないために、全体の分量比率を考え直すことになる。


<div class="nota">（原注）本文で述べたように、茶色いルーを作る際には澄ましバターを用いる。他の動
物性油脂はよほど経済的事情が｜逼迫《ひっぱく》していない限り使わないこと。材料コスト
が問題になる場合でも、ソースの仕上げに不純物を取り除く際に多少の注意を
払えば、ルーに用いたバターを回収するのはさして難しいことではない[^029]。それ
を後で他の用途で使えばいいだろう。</div><!--endNota-->

[^029]: エスコフィエ以前の料理書ではルーは白とブロンドがほとんど
    だった。エスコフィエの同時代をみるとルーの論点は色合いと油脂の種類
    に変化していく。デュボワとベルナール『キュイジーヌ・クラシック』
    （1856）ではバターの使用が指示されていた。グフェ『料理の本』(1867)
    でもルーによる[リエゾン\*](#lier-gls)としてやはりバターを使用する
    ことになっており、色合いもブロンドと白がほとんどだ。ところがエスコ
    フィエのほぼ同時代、ファーヴルの「事典」によると小麦粉をバターで赤
    茶色に熱したもの、と定義されている。オドの 1900 年版においてもバター
    の指定だが色は茶色になる。エスコフィエにおいてはすでに訳注で示した
    ように、バターか良質のグレスドマルミットという指定になる。これが第
    四版で澄ましバターのみを使用する指示に変化しているのは、原点回帰か、
    あるいは他の理由からか？ （たしかにルーにはバターを用いたほうが仕
    上がりがいい『ラルース・ガストロノミック』初版（1937）では、用途に
    応じて多少なりとも長時間、バターもしくは他の油脂で小麦粉を加熱した
    もの、という定義だ。つまりここでバター以外の油脂を使うことが想定さ
    れている。ペラプラ『近代調理技術』（1952）にいたっては、ルーにバター
    ではなく他の油脂を使うのは、バターが焦げやすくソースが苦い仕上りに
    なってしまうから、という理由でバターの使用を勧めてはいない。もっと
    も、ペラプラの場合、プチブルジョワの家庭料理本という性格がつよく、
    澄ましバターでじっくりと時間をかけてルーをつくるというのは読者にとっ
    て敷居の高いことだったろうとも想像される。初版〜第三版まではバター
    またはグレスドマルミットという指示であったことに留意する必要がある
    だろう。実際のところ、良質のバターを用いてルーを作ったほうが、軽や
    かな仕上りのソースになる傾向があることはいうまでもない。さて、ルー
    に用いたバターを回収するという点だが、おそらく、料理人の社会的地位
    向上に尽力したエスコフィエにおいてさえ、人的コストの概念
    は現在ほど厳密ではなかったように思われる。



\atoaki{}

#### ブロンドのルー {#roux-blond}

<div class="frsubenv">Roux blond</div>

\index{る@ルー!ふろんと@ブロンドの---}
\index{roux@roux!blond@--- blond}


* 仕上がり 1 kg 分


材料の比率は[茶色いルー](#roux-brun)と同じ。すなわちバター 500 g と、ふるった小麦粉 600 g。


火入れは、ルーがほんのりブロンド色になるまで、ごく弱火で行なう。






\atoaki{}

#### 白いルー {#roux-blanc}

<div class="frsubenv">Roux blanc</div>


\index{る@ルー!しろい@白い---}
\index{roux@roux!blanc@--- blanc}


500 gのバターと、ふるった小麦粉 600 g。


このルーの火入れは数分、つまり粉っぽさがなくなるまでの時間でいい。



[^024]: 現代の科学的見地からすると必ずしも正確な記述ではないので注意。


[^025]: 初版〜第三版では「澄ましバターまたは充分に澄ましたグレスド
    マルミット」となっている。グレスドマルミットとは、コンソメなどを作
    る際に、浮いてくる油脂を取り除く必要があるが、それを捨てずにまとめ
    てから漉して澄ませたもののこと。基本的に獣脂と考えていい。なお、同
    時代の料理書 --- 例えばペラプラ『近代料理技術』（1935年）--- には、
    ルーを作るのにバターを使う必要はなく、グレスドマルミットで充分、と
    しているものもある。


[^028]: 原文 chimiste。現代は分子ガストロノミーが盛んだが、料理を
    作る過程で起きる現象や結果を「化学」で説明しようとする試みは少なく
    ともカレームまで遡ることが出来る。[茶色いフォン](#fonds-brun-ou-estouffade)のレシピにおいて言
    及されるオスマゾームという想像上の物質もその範疇に含まれるだろう。
    また、化学の前身たる「錬金術」的概念は中世以来いくつかの料理書にお
    いて散見される。



</div><!--endRecette-->



<!--20190214江畑校正確認スミ-->
<!--20200225江畑校正確認スミ-->
